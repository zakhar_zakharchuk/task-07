package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    private static final String CONNECTION_URL_PROPERTY_NAME = "connection.url";
    private static final String CONNECTION_PROPERTIES_FILE_NAME = "app.properties";
    private final String url;

    private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users";
    private static final String SQL_INSERT_USER = "INSERT INTO users (login) VALUES(?)";
    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE id=?";
    private static final String SQL_GET_USER = "SELECT*FROM users WHERE login=?";
    private static final String SQL_GET_USER_TEAMS = "SELECT*FROM users_teams WHERE user_id=?";

    private static final String SQL_GET_TEAM = "SELECT*FROM teams WHERE name=?";
    private static final String SQL_FIND_ALL_TEAMS = "SELECT*FROM teams";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams (name) VALUES(?)";
    private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE id=?";
    private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";
    private static final String SQL_FIND_TEAM_BY_ID = "SELECT*FROM teams WHERE id=?";
    private static final String SQL_SET_TEAMS_FRO_USER="INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";


    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(CONNECTION_PROPERTIES_FILE_NAME)) {
            properties.load(reader);
            url = properties.getProperty(CONNECTION_URL_PROPERTY_NAME);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_FIND_ALL_USERS);
        ) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = mapUser(rs);
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DBException("Find all users failed", e);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)
        ) {
            st.setString(1, user.getLogin());
            int n = st.executeUpdate();
            if (n == 0) {
                throw new SQLException("0 rows affected");
            }
            try (ResultSet keys = st.getGeneratedKeys()) {
                if (keys.next()) {
                    user.setId(keys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed");
                }
            }
        } catch (SQLException e) {
            throw new DBException("Inserting failed", e);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        for (User user : users) {
            if (user == null)
                return false;
            try (Connection con = DriverManager.getConnection(url);
                 PreparedStatement st = con.prepareStatement(SQL_DELETE_USER)
            ) {
                st.setInt(1, user.getId());
                int n = st.executeUpdate();
                if (n == 0)
                    throw new SQLException("No teams to delete");
            } catch (SQLException e) {
                throw new DBException("Delete user failture", e);
            }
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = User.createUser(login);
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_GET_USER)
        ) {
            st.setString(1, login);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                user.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            throw new DBException("Get user failture", e);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = Team.createTeam(name);
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_GET_TEAM)
        ) {
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                team.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            throw new DBException("Get user failture", e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_FIND_ALL_TEAMS)
        ) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                teams.add(mapTeam(rs));
            }
        } catch (SQLException e) {
            throw new DBException("Find all teams failture", e);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)
        ) {
            st.setString(1, team.getName());
            int n = st.executeUpdate();
            if (n == 0)
                throw new SQLException("0 rows affected");
            try (ResultSet rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            throw new DBException("Inset team failture", e);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (user == null) throw new DBException("", new NullPointerException());
        try (Connection connection = DriverManager.getConnection(url);
        ) {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement(SQL_SET_TEAMS_FRO_USER)) {
                for (Team team : teams) {
                    if (team == null) {
                        connection.rollback();
                        throw new DBException("", new NullPointerException());
                    }
                    statement.setString(1, String.valueOf(user.getId()));
                    statement.setString(2, String.valueOf(team.getId()));
                    statement.executeUpdate();
                }
            } catch (SQLException throwables) {
                connection.rollback();
                throw new DBException("", throwables);
            }
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DBException("", throwables);
        }

        return true;

    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> usersTeams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_GET_USER_TEAMS)
        ) {
            st.setInt(1, user.getId());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("team_id");
                try (PreparedStatement st2 = con.prepareStatement(SQL_FIND_TEAM_BY_ID)) {
                    st2.setInt(1, id);
                    ResultSet rs2 = st2.executeQuery();
                    if (rs2.next()) {
                        usersTeams.add(mapTeam(rs2));
                    }
                }
            }
        } catch (SQLException e) {
            throw new DBException("Get user's teams failture", e);
        }
        return usersTeams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_DELETE_TEAM)
        ) {
            st.setInt(1, team.getId());
            int n = st.executeUpdate();
            if (n == 0)
                throw new SQLException("No teams to delete");
        } catch (SQLException e) {
            throw new DBException("Get team failture", e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement st = con.prepareStatement(SQL_UPDATE_TEAM)
        ) {
            st.setString(1, team.getName());
            st.setInt(2, team.getId());
            int n = st.executeUpdate();
            if (n == 0) {
                throw new SQLException("No team to update");
            }

        } catch (SQLException e) {
            throw new DBException("Update team failture", e);
        }
        return true;
    }

    private User mapUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }

    private Team mapTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }
}



